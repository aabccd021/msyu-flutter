import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:msyu/constant.dart';
import 'dart:convert';

import 'package:shared_preferences/shared_preferences.dart';

class Post {
  var semua;

  Post({this.semua});

  factory Post.fromJson(Map<String, dynamic> json) {
    return Post(semua: json["semua"]);
  }
}

class OrangTable extends StatefulWidget{
  @override
  OrangTableState createState() => OrangTableState();

}

class OrangTableState extends State<OrangTable> {

  String nama;

  Future<Null> getName() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      nama = prefs.getString("user");
    });
    print(nama);
  }

  @override
  void initState() {
    super.initState();
    nama = "";
    getName();
  }

  Future<Post> fetchPost() async {
    final response = await http.get('http://$hostname');

    if (response.statusCode == 200) {
      // If the call to the server was successful, parse the JSON
      print("ccd");
      return Post.fromJson(json.decode(response.body));
    } else {
      print(response.statusCode);
      // If that call was not successful, throw an error.
      throw Exception('Failed to load post');
    }
  }

  @override
  Widget build(BuildContext context) {
    Widget AnakBuilder(anak) {
      final TextTheme textTheme = Theme.of(context).textTheme;
      String nominal = anak[1];
      String jenis = anak[2];
      String waktu = anak[3];
      return Container(
        child: Padding(
          padding: const EdgeInsets.all(10.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Expanded(
                  flex: 1,
                  child: Text(
                    jenis,
                    style: textTheme.title,
                  )),
              Expanded(
                  flex: 1,
                  child: Text(
                    waktu,
//                    style: textTheme.title,
                  )),
              Expanded(
                  flex: 1,
                  child: Text(
                    nominal,
                    style: textTheme.title,
                    textAlign: TextAlign.right,
                  )),
            ],
          ),
        ),
        decoration: BoxDecoration(border: Border(bottom: BorderSide())),
      );
    }

    List<Widget> zaRisuto(table) {
      final TextTheme textTheme = Theme.of(context).textTheme;
//      List<Widget> a = table.map<Widget>((anak){return AnakBuilder(anak);}).toList();
      int total = 0;
      int talangan = 0;
      List<Widget> a = [];
      for (List row in table) {
        print(row[0]);
        print("nahloh==$nama");
        if (row[0] == nama) {
          print("masssssuk");
          a.add(AnakBuilder(row));
          talangan += int.parse(row[1]);
        }
        total += int.parse(row[1]);
      }
      total = (total / 8).round();
      int hutang = total - talangan;
      a.add(
          Container(
            child: Padding(
              padding: const EdgeInsets.all(10.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Expanded(
                      flex: 1,
                      child: Text(
                        "jenis",
                        style: textTheme.title,
                      )),
                  Expanded(
                      flex: 1,
                      child: Text(
                        "waktu",
//                        style: textTheme.title,
                      )),
                  Expanded(
                      flex: 1,
                      child: Text(
                        "nominal",
                        style: textTheme.title,
                        textAlign: TextAlign.right,
                      )),
                ],
              ),
            ),
            decoration: BoxDecoration(border: Border(bottom: BorderSide())),
          ));
      a.add(Text("total hutang : $hutang", style: textTheme.display1,));
      a.add(Text("total talangan : $talangan", style: textTheme.display1,));
      return a.reversed.toList();
    }

    return FutureBuilder<Post>(
      future: fetchPost(),
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          return ListView(
            children: zaRisuto(snapshot.data.semua),
          );
        } else if (snapshot.hasError) {
          return Text("${snapshot.error}");
        }
        return Center(child: CircularProgressIndicator());
      },
    );
  }
}
