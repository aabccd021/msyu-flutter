import 'package:flutter/material.dart';
import 'package:msyu/mainScreen.dart';
import 'package:shared_preferences/shared_preferences.dart';

class FirstScreen extends StatelessWidget {
  _pasPencet(String name) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setString('user', name);
  }

  @override
  Widget build(BuildContext context) {
    Widget _buildButton(String nama) {
      return RaisedButton(
        color: Colors.blue,
        child: Text(nama, style: TextStyle(color: Colors.white)),
        onPressed: () {
          _pasPencet(nama);
          Navigator.push(
              context, MaterialPageRoute(builder: (context) => MainScreen()));
          print("ccd");
        },
      );
    }

    return Scaffold(
//      appBar: AppBar(title: Text("Aabccd")),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
//        crossAxisAlignment: CrossAxisAlignment.end,
          children: <Widget>[
            Text("Anda siapa?", style: Theme.of(context).textTheme.display1),
            _buildButton("apoy"),
            _buildButton("cloud"),
            _buildButton("gio"),
            _buildButton("kani"),
            _buildButton("ariq"),
            _buildButton("gpr"),
            _buildButton("sidiq"),
            _buildButton("aab"),
          ],
        ),
      ),
    );
  }
}

