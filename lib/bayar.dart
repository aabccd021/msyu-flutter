import 'dart:convert';
import 'package:http/http.dart' as http;

import 'package:flutter/material.dart';
import 'package:msyu/constant.dart';

final Map<String, List> _jenisBayar = {
  "Listrik": [20000, 50000, 100000],
  "Air": [16000, 16500, 17000, 17500],
  "Internet": [388000, 390000, 405000, 408000, 423000],
  "Sampah": [50000],
  "Trashbag": [11000],
  "Gas": [50000]
};

Future fetchPost(jenis, nominal, nama) async {
  final response = await http.get(Uri.http(hostname, "/tambah",
      {"nama": nama, "jenis": jenis, "nominal": "$nominal"}));
//    final response = await http.get('http://172.20.203.209:5000/');

  if (response.statusCode == 200) {
    // If the call to the server was successful, parse the JSON
    print("ccd");
    print(response.body);
    return "OKgan";
  } else {
    print(response.statusCode);
    // If that call was not successful, throw an error.
    throw Exception('Failed to load post');
  }
}

kirim(jenis, nominal, nama, context) {
  showDialog(
      context: context,
      builder: (BuildContext cotext) {
        return FutureBuilder(
          future: fetchPost(jenis, nominal, nama),
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              return AlertDialog(
                title: new Text("Masuk pa eko"),
                content: new Text("berhasil bayar gan"),
                actions: <Widget>[
                  // usually buttons at the bottom of the dialog
                  new FlatButton(
                    child: new Text("Syap"),
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                  ),
                ],
              );
            } else if (snapshot.hasError) {
              return Text("${snapshot.error}");
            }
            return Center(child: CircularProgressIndicator());
          },
        );
      });
}

class ZaForm extends StatefulWidget {
  final String nama;
  ZaForm(this.nama);

  @override
  ZaFormState createState() {
    return ZaFormState(nama);
  }
}

class ZaFormState extends State<ZaForm> {
  final String nama;
  final _formKey = GlobalKey<FormState>();
  final jenisController = TextEditingController();
  final nominalController = TextEditingController();

  ZaFormState(this.nama);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
        width: 400,
        child: Form(
            key: _formKey,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                TextFormField(
                  decoration: InputDecoration(labelText: "jenis"),
                  controller: jenisController,
                  validator: (String value) {
                    if (value.isEmpty) {
                      return "isi cuk";
                    }
                  },
                ),
                TextFormField(
                  decoration: InputDecoration(labelText: "nominal"),
                  keyboardType: TextInputType.number,
                  controller: nominalController,
                  validator: (String value) {
                    if (value.isEmpty) {
                      return "isi cuk";
                    }
                  },
                ),
                Container(
                  width: double.infinity,
                  child: RaisedButton(
                    child: Text("Mbayar"),
                    onPressed: () {
                      // Validate will return true if the form is valid, or false if
                      // the form is invalid.
                      if (_formKey.currentState.validate()) {
//                     If the form is valid, we want to show a Snackbar
                        kirim(jenisController.text, nominalController.text,
                            nama, context);
//                       Scaffold.of(context)
//                        .showSnackBar(SnackBar(content: Text(nama)));
                      }
                    },
                  ),
                ),
              ],
            )),
      ),
    );
  }
}

class Bayyar extends StatelessWidget {
  String nama;

  Bayyar(this.nama);

  @override
  Widget build(BuildContext context) {
    final TextTheme textTheme = Theme.of(context).textTheme;

    Widget _bayarButtonBuilder2(jenis, nominal) {
      return RaisedButton(
        child: Text("$nominal"),
        onPressed: () => kirim(jenis, nominal, nama, context),
      );
    }

    List<Widget> _bayarWidgetList2(String jenis) {
      List<Widget> a = _jenisBayar[jenis]
          .map((nominal) => _bayarButtonBuilder2(jenis, nominal))
          .toList();
      a.insert(0, Text("Bayar $jenis", style: textTheme.display1));
      return a;
    }

    Widget lebihDetail(name) {
      return Scaffold(
          appBar: AppBar(title: Text("mbayar $name")),
          body: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: _bayarWidgetList2(name),
            ),
          ));
    }

    Widget _bayarButtonBuilder(name) {
      return RaisedButton(
          child: Text(name),
          onPressed: () {
            Navigator.push(context,
                MaterialPageRoute(builder: (context) => lebihDetail(name)));
          });
    }

    Widget dll() {
      return Scaffold(
          appBar: AppBar(
            title: Text("Mbayar yang lain lain"),
          ),
          body: ZaForm(nama));
    }

    List<Widget> _bayarWidgetList() {
      List<Widget> a =
          _jenisBayar.keys.map((jenis) => _bayarButtonBuilder(jenis)).toList();
      a.insert(0, Text("Bayar", style: textTheme.display1));
      a.add(RaisedButton(
          child: Text("lain lain"),
          onPressed: () {
            Navigator.push(
                context, MaterialPageRoute(builder: (context) => dll()));
          }));
      return a;
    }

    return Scaffold(
      appBar: AppBar(),
      body: Center(
        child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: _bayarWidgetList()),
      ),
    );
  }
}
