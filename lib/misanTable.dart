import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:msyu/constant.dart';
import 'dart:convert';

import 'package:msyu/mainScreen.dart';

class Post {
  var semua;

  Post({this.semua});

  factory Post.fromJson(Map<String, dynamic> json) {
    return Post(semua: json["semua"]);
  }
}

class MisanTable extends StatelessWidget {
//  @override
//  MisanTableState createState() => MisanTableState();
//}
//
//class MisanTableState extends State<MisanTable> {

  var semua;

  MisanTable({Key key}) : super(key: key);

//  @override
//  void initState() {
//    super.initState();
//    table = [
//      ["nanu"]
//    ];
//    makeRequest();
////  }
//

//  Widget listtt(){
//    return ListView(
//      children: zaRisuto(),
//    );
//  }

  Future<Post> fetchPost() async {
    final response = await http.get('http://$hostname');

    if (response.statusCode == 200) {
      // If the call to the server was successful, parse the JSON
      print("ccd");
      return Post.fromJson(json.decode(response.body));
    } else {
      print(response.statusCode);
      // If that call was not successful, throw an error.
      throw Exception('Failed to load post');
    }
  }

  @override
  Widget build(BuildContext context) {
    final TextTheme textTheme = Theme.of(context).textTheme;
    Widget DefaultPerOrang(semua) {
      print(semua[0][0]);
      int counter = 0;
      for (List row in semua) {
        counter += int.parse(row[1]);
      }
      counter = (counter / 8).round();
      return Text("default per orang : Rp $counter", style: textTheme.display1);
    }

    Widget AnakBuilder(anak) {
      String nama = anak[0];
      String nominal = anak[1];
      String jenis = anak[2];
      String waktu = anak[3];
      return Container(
        child: Padding(
          padding: const EdgeInsets.all(10.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Expanded(
                  flex: 1,
                  child: Text(
                    nama,
                    style: textTheme.title,
                  )),
              Expanded(
                  flex: 1,
                  child: Text(
                    jenis,
                    style: textTheme.title,
                  )),
              Expanded(
                  flex: 1,
                  child: Text(
                    waktu,
//                    style: textTheme.title,
                  )),
              Expanded(
                  flex: 1,
                  child: Text(
                    nominal,
                    style: textTheme.title,
                    textAlign: TextAlign.right,
                  )),
            ],
          ),
        ),
        decoration: BoxDecoration(border: Border(bottom: BorderSide())),
      );
    }

    List<Widget> zaRisuto(table) {
      List<Widget> a = table.map<Widget>((anak) => AnakBuilder(anak)).toList();
      a.add(Container(
        child: Padding(
          padding: const EdgeInsets.all(10.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Expanded(
                  flex: 1,
                  child: Text(
                    "nama",
                    style: textTheme.title,
                  )),
              Expanded(
                  flex: 1,
                  child: Text(
                    "jenis",
                    style: textTheme.title,
                  )),
              Expanded(
                  flex: 1,
                  child: Text(
                    "waktu",
//                    style: textTheme.title,
                  )),
              Expanded(
                  flex: 1,
                  child: Text(
                    "nominal",
                    style: textTheme.title,
                    textAlign: TextAlign.right,
                  )),
            ],
          ),
        ),
        decoration: BoxDecoration(border: Border.all()),
      ));
      a.add(DefaultPerOrang(table));
      return a.reversed.toList();
    }

    return FutureBuilder<Post>(
      future: fetchPost(),
      builder: (context, snapshot) {
        if (snapshot.hasData) {
//          return Column(
//            children: <Widget>[
//              DefaultPerOrang(snapshot.data.semua),
          return Column(
            children: zaRisuto(snapshot.data.semua),
          );
//            ],
//          );
//          return (snapshot.data.semua[0][0]);
        } else if (snapshot.hasError) {
          return Text("${snapshot.error}");
        }
        return Center(child: CircularProgressIndicator());
      },
    );
  }
}
