import 'package:flutter/material.dart';
import 'package:msyu/firstScreen.dart';
import 'package:msyu/mainScreen.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'bayar.dart';

void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  @override
  MyAppState createState() => MyAppState();
}

// This widget is the root of your application.
class MyAppState extends State<MyApp> {
  String _initialRoute = "/first";

  Future<Null> getName() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    if (prefs.getString("user") != null) {
      _initialRoute = '/';
    }
  }

  @override
  void initState() {
    super.initState();
    getName();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      initialRoute: _initialRoute,
      routes: {
        '/first': (context) => FirstScreen(),
        '/': (context) => MainScreen(),
      },
    );
  }
}


