import 'dart:convert';

import 'package:msyu/bayar.dart';
import 'package:msyu/misanTable.dart';
import 'package:msyu/orangTable.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter/material.dart';

class MainScreen extends StatefulWidget {
  @override
  MainScreenState createState() => MainScreenState();
}

class MainScreenState extends State<MainScreen> {
  static String nama;
  int _currentIndex = 0;

  List<Widget> _children = [
    OrangTable(),
    MisanTable(),
  ];

  void onTabTapped(int index) {
    setState(() {
      _currentIndex = index;
    });
  }


  Future<Null> getName() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      nama = prefs.getString("user");
    });
    print(nama);
  }

 @override
  void initState() {
    super.initState();
    nama = "";
    getName();
  }

  @override
  Widget build(BuildContext context) {
//    final TextTheme textTheme = Theme.of(context).textTheme;
    return Scaffold(
        appBar: AppBar(
          title: Text(nama),
          automaticallyImplyLeading: false,
          backgroundColor: Colors.blue,
        ),
        body: _children[_currentIndex],
        floatingActionButton: FloatingActionButton(
          onPressed: () => Navigator.push(
              context, MaterialPageRoute(builder: (context) => Bayyar(nama))),
          child: Icon(Icons.add),
        ),
        floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
        bottomNavigationBar: BottomAppBar(
            shape: CircularNotchedRectangle(),
            color: Colors.blue,
            child: new Row(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                IconButton(
                  icon: Icon(
                    Icons.person,
                    color: Colors.white,
                  ),
                  onPressed: () => onTabTapped(0),
                ),
                IconButton(
                  icon: Icon(
                    Icons.home,
                    color: Colors.white,
                  ),
                  onPressed: () => onTabTapped(1),
                ),
              ],
            )));
  }
}
